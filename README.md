# Base infrastructure for standing up a system in AWS for testing.

## Development

### Test the Terraform scripts

```shell
docker-compose --rm run terraform init
docker-compose --rm run terraform fmt
docker-compose --rm run terraform validate
```

### Validate resources using terraform plan in Dev

First time only

```shell
docker-compose --rm run terraform workspace new dev
docker-compose --rm run terraform plan
```

Every time after the first

```shell
docker-compose --rm run terraform workspace select dev
docker-compose --rm run terraform plan
```

### Stand-up the infrastructure using terraform

```shell
docker-compose --rm run terraform workspace select dev

docker-compose --rm run terraform apply
```

### Deployment

Deployment is achieved using Gitlab CI/CD capablity. The final job is a
manually executed destroy command that is only invoked at the maintainers
discretion.
