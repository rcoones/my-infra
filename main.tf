terraform {
  backend "s3" {
    bucket         = "my-infra-tfstate-rc"
    key            = "my-infra.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "my-infra-tfstate-lock-rc"
  }

  required_providers {
    aws = {
      version = "~> 2.54.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}
