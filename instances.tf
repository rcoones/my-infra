resource "aws_security_group" "allow_http_public" {
  name        = "allow_http"
  vpc_id      = aws_vpc.main.id
  description = "Allow HTTP public"

  ingress {
    description = "Allow traffic on port 80 to public"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-allow_http" })
  )
}

data "aws_ami" "httpd_server" {
  owners = ["self"]

  filter {
    name   = "name"
    values = ["BaseHTTPDImageWHostname"]
  }
}

resource "aws_instance" "webserver" {
  ami             = data.aws_ami.httpd_server.id
  subnet_id       = aws_subnet.public_a.id
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.allow_http_public.id]

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-webserver" })
  )
}
