variable "project" {
  default = "my-infra"
}

variable "contact" {
  default = "robert_coones@zohomail.com"
}

variable "prefix" {
  default = "my-infra"
}
